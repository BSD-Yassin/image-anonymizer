# PIL Image Anonymizer

## POC 
This project serves as a POC for parsing any photo with a noise filer that wouldn't change anything for the human eye but adds random information for an AI, so that it makes it impossible to retrace on the internet. I would like to use this personal project as a standard for any picture on my laptop or any computer I use. 

### How it works 

It decrypts the image through the PIL module and create a numpy dataset with Torch. So that the image becomes numerics values, the process of the gaussian filter (which is the first noise model I use) then goes to add "white" information on top of the image. One of the characteristics of gaussian white noise is its superposability, meaning it can be added on top without any alteration to the previous information. 

### How to install 

A requirements -r will be made soon enough. As it's not advanced enough at the moment, just create a manual venv and import *PIL*, *Numpy*, *Pytorch*


