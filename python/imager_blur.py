from asyncore import read
from hashlib import new
import matplotlib as plt
from fileinput import filename
import os
from PIL import ImageShow
from PIL import Image
from PIL import ImageFile
from perlin_noise import PerlinNoise
import magic
import torch
import cv2
import torchvision.transforms as transforms
import numpy as np
from PIL import Image
import time

class Target_Image():

    pwd = os.curdir
    fullpath = os.listdir

    def __init__(self,filename, path=pwd, output_name=filename, output_path=pwd):
        self.filename = filename
        self.image = os.open(self.filename, os.O_RDONLY)
        self.path = path
        self.format = magic.from_file(self.filename)
        self.output_name = output_name
        self.output_path = output_path

    def use_image():
        pass

    def treat_image():
        pass

    def scan_for_image():
        pass



stark = Target_Image('tony_stark.png')
#print(stark.__dict__)


def tensor_flow():
    image = cv2.imread(stark.filename)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    transform = transforms.Compose([
    transforms.ToTensor()
    ])
    revert = transforms.ToPILImage()
    # Convert the image to Torch tensor
    tensor = transform(image)

    print(tensor.shape)
    """ 
    new_tensor = []
    for i in range(1000):
        new_tensor = tensor
        noise = np.random.normal(0,0.0001,tensor.shape[2])
        new_tensor = np.add(tensor, noise)

    img = revert(new_tensor)

    img.save(f"{stark.filename}-gaussian.png", "PNG") """

def perlin_method():
    img = Image.open(stark.filename)
    noise = PerlinNoise(octaves=10, seed=1)
    xpix, ypix = img.size
    pic = [[noise([i/xpix, j/ypix]) for j in range(xpix)] for i in range(ypix)]